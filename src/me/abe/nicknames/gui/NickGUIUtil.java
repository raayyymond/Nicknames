package me.abe.nicknames.gui;

import me.abe.nicknames.Nicknames;
import me.abe.nicknames.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class NickGUIUtil {

    private static Nicknames plugin = Nicknames.getInstance();

    public static void show(Player player) {
        int pages = plugin.getNicknameManager().getNickAmt(player) / 36;
        int currentPage = 1;

        Listener listener = new Listener() {
            @EventHandler
            public void onClick(InventoryClickEvent e) {
                if(!(e.getClickedInventory().getName().equals("Nicknames"))) {
                    return;
                }

                Inventory inv = e.getClickedInventory();
            }
        };

    }

    public static Inventory getInv(Player player, int page) {
        Inventory inv = Bukkit.createInventory(null, 54, "Nicknames");

        ItemStack itemStack = new ItemStack(Material.SEEDS);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color("&8[&7Reveal Nicknames&8]"));
        meta.setLore(new ArrayList<String>() {{
            add(Utils.color("&8Reveal the true names"));
            add(Utils.color("&8of all the nicknamed players"));
            add(Utils.color("&8on your current server!"));
        }});
        itemStack.setItemMeta(meta);
        inv.setItem(45, itemStack);

        itemStack = new ItemStack(Material.BARRIER);
        meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color("&8[&7Remove Nickname&8]"));
        meta.setLore(new ArrayList<String>() {{
            add(Utils.color("&8Click here to &cremove &8your current nickname."));
        }});
        itemStack.setItemMeta(meta);
        inv.setItem(46, itemStack);

        itemStack = new ItemStack(Material.STAINED_GLASS_PANE, 1);
        meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color(""));
        meta.setLore(new ArrayList<>());
        itemStack.setItemMeta(meta);
        inv.setItem(47, itemStack);
        inv.setItem(51, itemStack);

        itemStack = new ItemStack(Material.PAPER);
        meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color("&8[&7Previous Page&8]"));
        meta.setLore(new ArrayList<>());
        itemStack.setItemMeta(meta);
        inv.setItem(48, itemStack);
        meta.setDisplayName(Utils.color("&8[&7Next Page&8]"));
        itemStack.setItemMeta(meta);
        inv.setItem(50, itemStack);

        itemStack = new ItemStack(Material.LEASH, 1);
        meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color("&8[&7Nickname Help&8]"));
        meta.setLore(new ArrayList<String>() {{
            add(Utils.color("&7To create a nickname use the command"));
            add(Utils.color("&b/nickname redeem <nickname>"));
            add(Utils.color("&7This will then be added to your nickname"));
            add(Utils.color("&7bank that can be accessed with"));
            add(Utils.color("&b/nickname"));
        }});
        itemStack.setItemMeta(meta);
        inv.setItem(52, itemStack);

        itemStack = new ItemStack(Material.BOOK, 1);
        meta = itemStack.getItemMeta();
        meta.setDisplayName(Utils.color("&8[&7Nickname Information&8]"));
        meta.setLore(new ArrayList<String>() {{
            add(Utils.color("&7Nicknames are a great way to add"));
            add(Utils.color("&bsome style to your chat! You can purchase nicknames at our webstore"));
            add(Utils.color("&7You can purchase each name "));
            add(Utils.color("&7bank that can be accessed with"));
            add(Utils.color("&b/nickname"));
        }});
        itemStack.setItemMeta(meta);
        inv.setItem(52, itemStack);

        return inv;
    }

}
