package me.abe.nicknames;

import me.abe.nicknames.cmd.NicknameCMD;
import me.abe.nicknames.nick.NickJoinListener;
import me.abe.nicknames.nick.NicknameManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Nicknames extends JavaPlugin {

    private static Nicknames instance;
    private Economy economy;
    private NicknameManager nicknameManager;

    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        economy = new Economy();
        nicknameManager = new NicknameManager();

        registerCommands();
        registerListeners();
    }

    private void registerCommands() {
        getCommand("nickname").setExecutor(new NicknameCMD());
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new NickJoinListener(), this);
    }

    public Economy getEconomy() {
        return economy;
    }

    public NicknameManager getNicknameManager() {
        return nicknameManager;
    }

    public static Nicknames getInstance() {
        return instance;
    }
}
