package me.abe.nicknames.cmd;

import me.abe.nicknames.Nicknames;
import me.abe.nicknames.gui.NickGUIUtil;
import me.abe.nicknames.msg.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NicknameCMD implements CommandExecutor {

    private Nicknames plugin;

    public NicknameCMD() {
        plugin = Nicknames.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(args.length == 0) {
            if(!(sender instanceof Player)) {
                sender.sendMessage(Message.MUST_BE_A_PLAYER.getMsg());
                return false;
            }

            Player player = (Player) sender;

            NickGUIUtil.show(player);
        } else if(args.length == 2) {
            if(args[0].equalsIgnoreCase("redeem")) {
                if(!(sender instanceof Player)) {
                    sender.sendMessage(Message.MUST_BE_A_PLAYER.getMsg());
                    return false;
                }

                Player player = (Player) sender;

                if(!player.hasPermission("nickname.redeem")) {
                    player.sendMessage(Message.NO_PERMISSION.getMsg());
                    return false;
                }

                if(plugin.getNicknameManager().hasNick(player, args[1])) {
                    player.sendMessage(Message.ALREADY_HAVE_NICK.getMsg());
                    return false;
                }

                if(plugin.getEconomy().hasEnough(player.getUniqueId(), plugin.getConfig().getInt("price"))) {
                    if(args[1].matches("[\\p{Alnum}]+")) {
                        plugin.getNicknameManager().createNewNick(player, args[1]);
                        player.sendMessage(Message.REEDEMED.getMsg().replace("%nick%", args[1]));
                    } else {
                        player.sendMessage(Message.INVALID_NICKNAME.getMsg());
                    }
                } else {
                    player.sendMessage(Message.INSUFFICIENT_FUNDS.getMsg());
                }
            } else {
                sender.sendMessage(Message.CORRECT_USAGE.getMsg());
            }
        } else if(args.length == 3) {
            if(args[0].equalsIgnoreCase("give")) {
                if(!sender.hasPermission("nickname.give")) {
                    sender.sendMessage(Message.NO_PERMISSION.getMsg());
                    return false;
                }

                if(Bukkit.getPlayer(args[1]) == null) {
                    sender.sendMessage(Message.NOT_ONLINE.getMsg());
                } else {
                    if (args[2].matches("[0-9]+")) {
                        plugin.getEconomy().deposit(Bukkit.getPlayer(args[1]).getUniqueId(), Integer.parseInt(args[2]));
                        sender.sendMessage(Message.GIVEN.getMsg().replace("%player%", args[1]).replace("%amt%", args[2]));
                    } else {
                        sender.sendMessage(Message.INVALID_NUMBER.getMsg());
                    }
                }
            } else if(args[0].equalsIgnoreCase("remove")) {
                if(!sender.hasPermission("nickname.remove")) {
                    sender.sendMessage(Message.NO_PERMISSION.getMsg());
                    return false;
                }

                if(Bukkit.getPlayer(args[1]) == null) {
                    sender.sendMessage(Message.NOT_ONLINE.getMsg());
                } else {
                    if (args[2].matches("[0-9]+")) {
                        plugin.getEconomy().withdraw(Bukkit.getPlayer(args[1]).getUniqueId(), Integer.parseInt(args[2]));
                        sender.sendMessage(Message.REMOVED.getMsg().replace("%player%", args[1]).replace("%amt%", args[2]));
                    } else {
                        sender.sendMessage(Message.INVALID_NUMBER.getMsg());
                    }
                }
            }
        } else {
            sender.sendMessage(Message.CORRECT_USAGE.getMsg());
        }

        return true;
    }
}
