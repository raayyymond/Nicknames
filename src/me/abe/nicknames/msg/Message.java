package me.abe.nicknames.msg;

import me.abe.nicknames.utils.FileUtils;
import me.abe.nicknames.utils.Utils;
import org.bukkit.configuration.file.FileConfiguration;

public enum Message {

    MUST_BE_A_PLAYER("&cYou must be a player in order to run this command!"),
    CORRECT_USAGE("&cCorrect Usage: /nickname (redeem/give/remove) (nickname/player/player) (amount)"),
    NO_PERMISSION("&cYou do not have permission to run this command!"),
    INSUFFICIENT_FUNDS("&cYou do not have enough funds!"),
    REEDEMED("&aYou have redeemed the nickname %nick%!"),
    INVALID_NICKNAME("&cYour nickname must contain only alphanumeric characters!"),
    INVALID_NUMBER("&cInvalid number!"),
    NOT_ONLINE("&cThat player is not online!"),
    GIVEN("&aYou have given %player% %amt% nickname credits!"),
    REMOVED("&aYou have removed %amt% nickname credits from %player%!"),
    ALREADY_HAVE_NICK("&cYou already have this nickname!");

    String msg;
    Message(String string) {
        msg = string;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return Utils.color(msg);
    }
    public String getRawMsg() {
        return msg;
    }
    public static void loadMessages() {
        FileConfiguration config = FileUtils.getFile("messages.yml");
        for(Message msg : Message.values()) {
            if(config.contains("messages." + msg.toString()))
                msg.setMsg(config.getString("messages." + msg.toString()));
            else
                config.set("messages." + msg.toString(), msg.getRawMsg());
        }
        FileUtils.saveFile(config, "messages.yml");
    }
    public String toString() {
        return name();
    }

}
