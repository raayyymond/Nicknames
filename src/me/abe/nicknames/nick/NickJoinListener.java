package me.abe.nicknames.nick;

import me.abe.nicknames.Nicknames;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class NickJoinListener implements Listener {

    private Nicknames plugin;

    public NickJoinListener() {
        plugin = Nicknames.getInstance();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        if(plugin.getNicknameManager().hasNick(e.getPlayer())) {
            e.getPlayer().setDisplayName(plugin.getNicknameManager().getActiveNick(e.getPlayer()));
        }
    }

}
