package me.abe.nicknames.nick;

import me.abe.nicknames.utils.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NicknameManager {

    private Map<UUID, List<String>> nicknames;

    public NicknameManager() {
        loadNicknames();
    }

    public String getActiveNick(Player player) {
        UUID uuid = player.getUniqueId();
        FileConfiguration nickConfig = FileUtils.getFile("nicknames.yml");
        return nicknames.get(uuid).get(nickConfig.getInt(uuid.toString() + ".active"));
    }

    public void setActiveNick(Player player, String nick) {
        UUID uuid = player.getUniqueId();
        FileConfiguration nickConfig = FileUtils.getFile("nicknames.yml");
        nickConfig.set(uuid.toString() + ".active", nicknames.get(uuid).indexOf(nick));
    }

    public void createNewNick(Player player, String nick) {
        UUID uuid = player.getUniqueId();
        List<String> nicks = nicknames.get(uuid);
        nicks.add(nick);
        nicknames.put(uuid, nicks);
        updateFile();

        setActiveNick(player, nick);
    }

    public boolean hasNick(Player player, String nick) {
        return hasNick(player) && nicknames.get(player.getUniqueId()).contains(nick);
    }

    public boolean hasNick(Player player) {
        return nicknames.containsKey(player.getUniqueId());
    }

    public int getNickAmt(Player player) {
        return hasNick(player) ? nicknames.get(player).size() : 0;
    }

    private void loadNicknames() {
        nicknames = new HashMap<>();
        FileConfiguration nickConfig = FileUtils.getFile("nicknames.yml");
        for(String rawUUID : nickConfig.getConfigurationSection("").getKeys(false)) {
            UUID uuid = UUID.fromString(rawUUID);
            nicknames.put(uuid, nickConfig.getStringList(rawUUID));
        }
    }

    private void updateFile() {
        FileConfiguration ecoConfig = FileUtils.getFile("nicknames.yml");
        for(UUID uuid : nicknames.keySet())
            ecoConfig.set(uuid.toString(), nicknames.get(uuid));
        FileUtils.saveFile(ecoConfig, "nicknames.yml");
    }

}
